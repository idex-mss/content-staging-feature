<?php
/**
 * @file
 * content_staging.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function content_staging_default_rules_configuration() {
  $items = array();
  $items['rules_push_node_to_target'] = entity_import('rules_config', '{ "rules_push_node_to_target" : {
      "LABEL" : "Push Node to Target",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "deploy" ],
      "ON" : { "node_update" : [], "node_insert" : [] },
      "IF" : [
        { "component_rules_user_is_not_deploy_user" : { "user" : [ "site:current-user" ] } }
      ],
      "DO" : [
        { "deploy_rules_action_add_to_managed_plan" : { "plan_name" : "push_to_target", "entity" : [ "node" ] } }
      ]
    }
  }');
  $items['rules_push_taxonomy_term_to_target'] = entity_import('rules_config', '{ "rules_push_taxonomy_term_to_target" : {
      "LABEL" : "Push Taxonomy Term to Target",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "deploy", "taxonomy" ],
      "ON" : { "taxonomy_term_update" : [], "taxonomy_term_insert" : [] },
      "IF" : [
        { "component_rules_user_is_not_deploy_user" : { "user" : [ "site:current-user" ] } }
      ],
      "DO" : [
        { "deploy_rules_action_add_to_managed_plan" : { "plan_name" : "push_to_target", "entity" : [ "term" ] } }
      ]
    }
  }');
  $items['rules_user_is_not_deploy_user'] = entity_import('rules_config', '{ "rules_user_is_not_deploy_user" : {
      "LABEL" : "User is not Deploy user",
      "PLUGIN" : "and",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "user" : { "label" : "User", "type" : "user" } },
      "AND" : [
        { "NOT user_has_role" : { "account" : [ "user" ], "roles" : { "value" : { "6" : "6" } } } }
      ]
    }
  }');
  return $items;
}
