<?php
/**
 * @file
 * content_staging.deploy_plans.inc
 */

/**
 * Implements hook_deploy_plans_default().
 */
function content_staging_deploy_plans_default() {
  $export = array();

  $plan = new DeployPlan();
  $plan->disabled = FALSE; /* Edit this to true to make a default plan disabled initially */
  $plan->api_version = 1;
  $plan->name = 'push_to_target';
  $plan->title = 'Push to Target';
  $plan->description = '';
  $plan->debug = 0;
  $plan->aggregator_plugin = 'DeployAggregatorManaged';
  $plan->aggregator_config = array(
    'delete_post_deploy' => 1,
  );
  $plan->fetch_only = 0;
  $plan->processor_plugin = 'DeployProcessorQueue';
  $plan->processor_config = array();
  $plan->endpoints = array(
    'target' => 'target',
  );
  $plan->dependency_plugin = 'deploy_iterator';
  $export['push_to_target'] = $plan;

  return $export;
}
