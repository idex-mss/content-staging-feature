<?php
/**
 * @file
 * content_staging.deploy_endpoints.inc
 */

/**
 * Implements hook_deploy_endpoints_default().
 */
function content_staging_deploy_endpoints_default() {
  
  global $conf;
  $url = $conf['content_staging']['url'];
  $username = $conf['content_staging']['username'];
  $password = $conf['content_staging']['password'];

  $export = array();

  $endpoint = new DeployEndpoint();
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 1;
  $endpoint->name = 'target';
  $endpoint->title = 'target';
  $endpoint->description = '';
  $endpoint->debug = 0;
  $endpoint->authenticator_plugin = 'DeployAuthenticatorSession';
  $endpoint->authenticator_config = array(
    'username' => $username,
    'password' => $password,
  );
  $endpoint->service_plugin = 'DeployServiceRestJSON';
  $endpoint->service_config = array(
    'url' => $url,
  );
  $export['target'] = $endpoint;

  return $export;
}
