**Here is a comprehensive list of projects and the corresponding module names that should be enabled for this feature to function.**

```
drush dl rules services ctools deploy deploy_plus entity uuid file_entity views entity_dependency
```

rules
- rules
- rules_admin
services
- services
- rest_server
ctools
- ctools
deploy
- deploy
- deploy_ui
deploy_plus
- deploy_plus
entity
- entity
uuid
- uuid
- uuid_services
file_entity
- file_entity
views
- views
- views_ui
entity_dependency    
- entity_dependency


The following should be added to your custom deployment module.install file as an update HOOK.
Update the function name according to https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_update_N/7.x

```
/**
 * Enable Content Staging features.
 */
function deployment_update_7000() {
    global $conf;
    if(isset($conf['content_staging'])) {
        module_enable(array('content_staging'));
    }
    else {
        throw new DrupalUpdateException('You must define the content_staging options within settings.php');
    }
}
```